{{/* Generate basic labels. */}}
{{- define "labels" }}
labels:
  app.kubernetes.io/managed-by: {{ $.Release.Service }}
  helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version }}
{{- end }}
